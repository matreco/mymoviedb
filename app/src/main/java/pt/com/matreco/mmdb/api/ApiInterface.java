package pt.com.matreco.mmdb.api;

import pt.com.matreco.mmdb.api.model.movies.MovieDetails;
import pt.com.matreco.mmdb.api.model.movies.MovieDetailsCredits;
import pt.com.matreco.mmdb.api.model.movies.MovieResults;
import pt.com.matreco.mmdb.api.model.movies.MovieTrailer;
import pt.com.matreco.mmdb.api.model.tvSeries.TvDetails;
import pt.com.matreco.mmdb.api.model.tvSeries.TvResults;
import pt.com.matreco.mmdb.api.model.tvSeries.TvSerieDetailsCredits;
import pt.com.matreco.mmdb.api.model.tvSeries.TvSerieSeasons;
import pt.com.matreco.mmdb.api.model.tvSeries.TvTrailer;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by carlo on 06/12/2017.
 */

public interface ApiInterface {
    /****************
     MOVIES
     ****************/

    @GET("/3/movie/{category}")
    Call<MovieResults> getListOfMovies(
            @Path("category") String category,
            @Query("api_key") String apiKey,
            @Query("language") String language,
            @Query("page") int page
    );

    //  3/movie/13?api_key=607def6&language=en-US
    @GET("/3/movie/{movie_id}")
    Call<MovieDetails> getMovieDetails(
            @Path("movie_id") String movieId,
            @Query("api_key") String apiKey,
            @Query("language") String language
    );

    //  3/movie/284053/credits?api_key=36607d
    @GET ("3/movie/{movie_id}/credits")
    Call<MovieDetailsCredits> getMovieDetailsCredits(
            @Path("movie_id") String movieId,
            @Query("api_key") String apiKey
    );

    //  3/movie/13/videos?api_key=36607def6&language=en-US
    @GET ("3/movie/{movie_id}/videos")
    Call<MovieTrailer> getMovieTrailer(
            @Path("movie_id") String movieId,
            @Query("api_key") String apiKey,
            @Query("language") String language
    );

    // 3/search/movie?api_key=694536607def6&language=en-US&query=Forest&page=1&include_adult=false
    @GET ("3/search/movie")
    Call<MovieResults> getSearchMovie(
            @Query("api_key") String apiKey,
            @Query("language") String language,
            @Query("query") String movieString,
            @Query("page") int page
    );

// /3/tv/popular?api_key=36607def6&language=en-US&page=1
    /****************
     TV SERIES
     ****************/

    @GET("3/tv/{category}")
    Call<TvResults> getListOfTvSeries(
            @Path("category") String category,
            @Query("api_key") String apiKey,
            @Query("language") String language,
            @Query("page") int page
    );

    // 3/tv/1418?api_key=536607def6&language=en-US
    @GET("/3/tv/{tvSerie_id}")
    Call<TvDetails> getTvSerieDetails(
            @Path("tvSerie_id") String tvSerieId,
            @Query("api_key") String apiKey,
            @Query("language") String language
    );

    //  3/tv/1418/videos?api_key=66607def6&language=en-US
    @GET ("3/tv/{tvSerie_id}/videos")
    Call<TvTrailer> getTvSerieTrailer(
            @Path("tvSerie_id") String tvSerieId,
            @Query("api_key") String apiKey,
            @Query("language") String language
    );
    //   3/tv/1418/credits?api_key=607def6&language=en-US
    @GET ("3/tv/{tvSerie_id}/credits")
    Call<TvSerieDetailsCredits> getTvSerieDetailsCredits(
            @Path("tvSerie_id") String tvSerieId,
            @Query("api_key") String apiKey
    );

    //    https://api.themoviedb.org/3/search/tv?api_key=<<api_key>>&language=en-US&page=1
    @GET ("3/search/tv")
    Call<TvResults> getSearchTvSeries(
            @Query("api_key") String apiKey,
            @Query("language") String language,
            @Query("query") String tvSerieString,
            @Query("page") int page
    );

    //https://api.themoviedb.org/3/tv/60625/season/1?api_key=6536607def6&language=en-US
    @GET ("3/tv/{tv_id}/season/{season_number}")
    Call<TvSerieSeasons> getTvSeriesSeasons(
            @Path("tv_id") String tvSerieString,
            @Path("season_number") Integer tvSerieSeasonNumber,
            @Query("api_key") String apiKey,
            @Query("language") String language
    );

}
