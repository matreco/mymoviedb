
package pt.com.matreco.mmdb.api.model.tvSeries;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TvTrailer {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("results")
    @Expose
    private List<TvTrailerResult> results = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<TvTrailerResult> getResults() {
        return results;
    }

    public void setResults(List<TvTrailerResult> results) {
        this.results = results;
    }

}
