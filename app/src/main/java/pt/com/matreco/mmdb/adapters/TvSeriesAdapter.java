package pt.com.matreco.mmdb.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import pt.com.matreco.mmdb.R;
import pt.com.matreco.mmdb.api.model.movies.Results;
import pt.com.matreco.mmdb.api.model.tvSeries.Result;

/**
 * Created by carlo on 10/12/2017.
 */

public class TvSeriesAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;
    private List<Result> mDataSource;

    public TvSeriesAdapter(Context context, List<Result> items) {
        mContext = context;
        mDataSource = items;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return mDataSource.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataSource.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position % 2;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int layoutResource = 0; // determined by view type
        int viewType = getItemViewType(position);
//        switch (viewType) {
//            case 0:
//                layoutResource = R.layout.screen_tvseries_even_listitems;
//                break;
//
//            case 1:
//                layoutResource = R.layout.screen_tvseries_odd_listitems;
//                break;
//        }

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.screen_tvseries_listitems, parent, false);
        }


        Result r = (Result) getItem(position);

        TextView titleTextView = convertView.findViewById(R.id.tvseries_title);
        ImageView thumbnailImageView = convertView.findViewById(R.id.tvseries_thumbnail);
        TextView releaseDateView = convertView.findViewById(R.id.tvseries_list_item_releaseDate);
//        TextView popularityView = convertView.findViewById(R.id.tvseries_list_item_popularity);
        RatingBar ratingBar = convertView.findViewById(R.id.ratingSeries);

        titleTextView.setText(r.getName());
        releaseDateView.setText(r.getFirstAirDate());
//        popularityView.setText(getPopularityString(r.getVoteAverage()));
        Picasso.with(mContext).load(getFullImageUrl(r)).placeholder(R.mipmap.mmdb_ic_launcher_round).into(thumbnailImageView);

        float rating= (float) ((r.getVoteAverage()*5) /10);
        ratingBar.setRating(rating);

        return convertView;
    }

    private String getPopularityString(double popularity) {
        java.text.DecimalFormat decimalFormat = new java.text.DecimalFormat("#.#");
        return decimalFormat.format(popularity);
    }

    private String getFullImageUrl(Result movie) {
        String imagePath;

        if (movie.getPosterPath() != null && !movie.getPosterPath().isEmpty()) {
            imagePath = movie.getPosterPath();
        } else {
            imagePath = movie.getBackdropPath();
        }
        return "https://image.tmdb.org/t/p/w500" + imagePath;
    }

    public void add(Result r) {
        mDataSource.add(r);
        notifyDataSetChanged();
    }

    public void addAll(List<Result> tvSeriesResults) {
        for (Result result : tvSeriesResults) {
            add(result);
        }
    }

    public void clearData() {
        mDataSource.clear();
    }
}
