package pt.com.matreco.mmdb.splash;

import android.animation.Animator;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.ListView;

import butterknife.BindView;
import butterknife.ButterKnife;
import pt.com.matreco.mmdb.MainActivity;
import pt.com.matreco.mmdb.R;

/**
 * Created by carlo on 05/12/2017.
 */

public class SplashActivity extends Screen {
    @BindView(R.id.splash_screen_image)
    ImageView splashImageView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_main_splash_activity);
        ButterKnife.bind(this);
        startAnimation();
    }

    private void startAnimation() {
        splashImageView.setScaleX(0.2f);
        splashImageView.setScaleY(0.2f);
        splashImageView.setAlpha(0f);

        splashImageView.animate()
                .alpha(1f)
                .scaleX(1f)
                .scaleY(1f)
                .rotation(990)
                .setDuration(3100)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);

                        finish();
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animator) {

                    }
                })
                .start();
    }

}
