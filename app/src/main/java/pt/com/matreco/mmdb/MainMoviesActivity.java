package pt.com.matreco.mmdb;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pt.com.matreco.mmdb.adapters.MovieAdapter;
import pt.com.matreco.mmdb.api.ApiInterface;
import pt.com.matreco.mmdb.api.model.movies.MovieResults;
import pt.com.matreco.mmdb.api.model.movies.Results;
import pt.com.matreco.mmdb.details.MainMovieDetails;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by carlo on 09/12/2017.
 */
public class MainMoviesActivity extends AppCompatActivity {
    @BindView(R.id.loader)
    View loadingView;
    @BindView(R.id.list_item)
    GridView list_item;

    SwipeRefreshLayout mSwipeRefreshLayout;

    List<Results> rlistOfMovies  = new ArrayList<>();
    int mRPage= 1;

    MovieAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_movies_listview);
        ButterKnife.bind(this);

        mSwipeRefreshLayout = findViewById(R.id.activity_main_swipe_refresh_layout);
        adapter = new MovieAdapter(this,rlistOfMovies);
        list_item.setAdapter(adapter);

        list_item.setOnScrollListener(new EndlessScrollListener(5) {
            @Override
            public void loadMore(int page, int totalItemsCount) {
                mRPage++;
                loadMoreMovies(mRPage);

            }
        });

        firstLoad(mRPage);
        adapter.notifyDataSetChanged();

        // ListView Item Click Listener
        list_item.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Results r = rlistOfMovies.get(position);
                Intent myIntent = new Intent(MainMoviesActivity.this, MainMovieDetails.class);
                myIntent.putExtra("title", r.getTitle());
                myIntent.putExtra("movieId", r.getId().toString());
                startActivity(myIntent);
            }

        });

        // TODO atualizar este codigo
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                adapter.clearData();
                adapter = new MovieAdapter(getBaseContext(), rlistOfMovies);
                list_item.setAdapter(adapter);
                mRPage = 1;
                firstLoad(mRPage);
                adapter.notifyDataSetChanged();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });


    }

    public void loadMoreMovies(int page){
        String CATEGORY = this.getIntent().getExtras().getString("category");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.API_URL))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiInterface apiI = retrofit.create(ApiInterface.class);
        Call<MovieResults> callMovieResults = apiI.getListOfMovies(CATEGORY, getString(R.string.API_KEY), getString(R.string.API_LANGUAGE), page);
        callMovieResults.enqueue(new Callback<MovieResults>() {
            @Override
            public void onResponse(Call<MovieResults> call, Response<MovieResults> response) {
                final MovieResults mResults = response.body();
                List<Results> listOfMovies = mResults.getResults();

                Log.d("TAG","Página "+ mResults.getPage() + " Total Pages " + mResults.getTotalPages() + "tamanho" + mResults.getResults().size());
                Log.d("TAG","NOVA PAGINA "+ mRPage);

                stopProgressBar();
                adapter.addAll(listOfMovies);
            }

            @Override
            public void onFailure(Call<MovieResults> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void firstLoad(int page){
        String CATEGORY = this.getIntent().getExtras().getString("category");
        String TYPE = this.getIntent().getExtras().getString("type");
        String MOVIESTRING = this.getIntent().getExtras().getString("movieString");

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.API_URL))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface apiI = retrofit.create(ApiInterface.class);

        if(TYPE != null && !TYPE.isEmpty()){
            Call<MovieResults> callMovieResults = apiI.getSearchMovie(getString(R.string.API_KEY), getString(R.string.API_LANGUAGE),MOVIESTRING, mRPage);
            callMovieResults.enqueue(new Callback<MovieResults>() {
                @Override
                public void onResponse(Call<MovieResults> call, Response<MovieResults> response) {
                    final MovieResults mResults = response.body();

                    if(mResults.getTotalResults()==0){
                        Intent myIntent = new Intent(MainMoviesActivity.this, MainMoviesMenuActivity.class);
                        startActivity(myIntent);
                        finish();

                    }
                    List<Results> listOfMovies = mResults.getResults();
                    adapter.addAll(listOfMovies);
                }

                @Override
                public void onFailure(Call<MovieResults> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        }else {
            Call<MovieResults> callMovieResults = apiI.getListOfMovies(CATEGORY, getString(R.string.API_KEY), getString(R.string.API_LANGUAGE), mRPage);
            callMovieResults.enqueue(new Callback<MovieResults>() {
                @Override
                public void onResponse(Call<MovieResults> call, Response<MovieResults> response) {
                    final MovieResults mResults = response.body();
                    List<Results> listOfMovies = mResults.getResults();

                    stopProgressBar();
                    adapter.addAll(listOfMovies);
                }

                @Override
                public void onFailure(Call<MovieResults> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }


    public void stopProgressBar() {
        loadingView.setVisibility(View.GONE);
        showContent(true);
    }

    private void showContent(boolean show) {
        int visibility = show ? View.VISIBLE : View.INVISIBLE;
        list_item.setVisibility(visibility);
    }
}
