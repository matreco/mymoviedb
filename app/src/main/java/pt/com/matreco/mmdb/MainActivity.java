package pt.com.matreco.mmdb;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_main_menu_activity);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_movies)
    public void submitMovies() {
        Intent intent = new Intent(this, MainMoviesMenuActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_tvSeries)
    public void submitTvSeries() {
        Intent intent = new Intent(this, MainTvSeriesMenuActivity.class);
        startActivity(intent);
    }

}
