package pt.com.matreco.mmdb.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import pt.com.matreco.mmdb.MainTvSeriesActivity;
import pt.com.matreco.mmdb.MainTvSeriesSeasonDetailsActivity;
import pt.com.matreco.mmdb.R;

/**
 * Created by carlo on 01/01/2018.
 */

public class HorizontalLayoutForSeasonsAdapter extends LinearLayout {

    Context myContext;
    ArrayList<String> itemList = new ArrayList<String>();

    public HorizontalLayoutForSeasonsAdapter(Context context) {
        super(context);
        myContext = context;
    }

    public HorizontalLayoutForSeasonsAdapter(Context context, AttributeSet attrs) {
        super(context, attrs);
        myContext = context;
    }

    public HorizontalLayoutForSeasonsAdapter(Context context, AttributeSet attrs,
                                             int defStyle) {
        super(context, attrs, defStyle);
        myContext = context;
    }

    public void add(String path, String tvSerieId) {
        int newIdx = itemList.size();
        itemList.add(path);
        //TODO workarround -- arranjar forma mais elegante
        addView(getImageView(newIdx+1,path,tvSerieId));
    }



    ImageView getImageView(final int seasonNumber, final String path, final String tvSerieId) {
        Bitmap bm = null;
        if (seasonNumber < itemList.size()) {
            bm = decodeSampledBitmapFromUri(itemList.get(seasonNumber), 520, 520);
        }

        ImageView imageView = new ImageView(myContext);
        imageView.setLayoutParams(new LayoutParams(520, 520));
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        imageView.setImageBitmap(bm);

        Picasso.with(myContext).load(path)
                .placeholder(R.mipmap.mmdb_ic_launcher_round)
                .into(imageView);


        imageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(myContext, MainTvSeriesSeasonDetailsActivity.class);
                intent.putExtra("tvSerieId", tvSerieId);
                intent.putExtra("seasonNumber", seasonNumber+"");
                myContext.startActivity(intent);
//                Toast.makeText(myContext,
//                        "Clicked - " + i +" path " +path + " tvSerieId " +tvSerieId,
//                        Toast.LENGTH_LONG).show();
            }
        });

        return imageView;
    }

    public Bitmap decodeSampledBitmapFromUri(String path, int reqWidth, int reqHeight) {
        Bitmap bm = null;

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(path, options);

        return bm;
    }

    public int calculateInSampleSize(

            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }

        return inSampleSize;
    }

}