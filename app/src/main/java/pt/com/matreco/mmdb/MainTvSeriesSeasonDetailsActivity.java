package pt.com.matreco.mmdb;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pt.com.matreco.mmdb.adapters.SeasonDetailsAdapter;
import pt.com.matreco.mmdb.adapters.TvSeriesAdapter;
import pt.com.matreco.mmdb.api.ApiInterface;
import pt.com.matreco.mmdb.api.model.tvSeries.Episode;
import pt.com.matreco.mmdb.api.model.tvSeries.Result;
import pt.com.matreco.mmdb.api.model.tvSeries.TvResults;
import pt.com.matreco.mmdb.api.model.tvSeries.TvSerieSeasons;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by carlo on 04/01/2018.
 */

public class MainTvSeriesSeasonDetailsActivity extends AppCompatActivity {
    @BindView(R.id.season_episode_list_item)
    ListView list_item;


    List<Episode> rlistOfTvSeries  = new ArrayList<>();
    SeasonDetailsAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.screen_tvseries_season_listview);
        ButterKnife.bind(this);

        String TVSERIEID = this.getIntent().getExtras().getString("tvSerieId");
        String SEASON_NUMBER = this.getIntent().getExtras().getString("seasonNumber");

//        Log.d("SEASON", "-------------> " + TVSERIEID + " ------- > " + SEASON_NUMBER);

        adapter = new SeasonDetailsAdapter(this,rlistOfTvSeries);
        list_item.setAdapter(adapter);
        getTvSeriesSeasonPoster(SEASON_NUMBER, TVSERIEID);
        adapter.notifyDataSetChanged();

    }

    private void getTvSeriesSeasonPoster(String tvSerieId, String tvSerieSeasonNumber) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.API_URL))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface apiI = retrofit.create(ApiInterface.class);
        Call<TvSerieSeasons> callTvSerieSeasons = apiI.getTvSeriesSeasons(tvSerieSeasonNumber,Integer.parseInt(tvSerieId), getString(R.string.API_KEY), getString(R.string.API_LANGUAGE));
//        Log.d("Season Number", "----> " + tvSerieSeasonNumber);
        callTvSerieSeasons.enqueue(new Callback<TvSerieSeasons>() {
            @Override
            public void onResponse(Call<TvSerieSeasons> call, Response<TvSerieSeasons> response) {
                TvSerieSeasons rTvSeriesDetails = response.body();
//                Log.d("Response", "----> " + response.raw());
                List<Episode> listOfEpisodes = rTvSeriesDetails.getEpisodes();
                adapter.addAll(listOfEpisodes);
            }

            @Override
            public void onFailure(Call<TvSerieSeasons> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

}
