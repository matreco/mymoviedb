package pt.com.matreco.mmdb.details;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.view.ViewGroup.LayoutParams;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.com.matreco.mmdb.MainMoviesActivity;
import pt.com.matreco.mmdb.MainMoviesMenuActivity;
import pt.com.matreco.mmdb.R;
import pt.com.matreco.mmdb.adapters.HorizontalLayoutForSeasonsAdapter;
import pt.com.matreco.mmdb.api.ApiInterface;
import pt.com.matreco.mmdb.api.model.movies.Cast;
import pt.com.matreco.mmdb.api.model.movies.Genre;
import pt.com.matreco.mmdb.api.model.movies.MovieDetails;
import pt.com.matreco.mmdb.api.model.movies.MovieDetailsCredits;
import pt.com.matreco.mmdb.api.model.movies.MovieTrailer;
import pt.com.matreco.mmdb.api.model.movies.MovieTrailerResult;
import pt.com.matreco.mmdb.api.model.movies.Results;
import pt.com.matreco.mmdb.api.model.tvSeries.Crew;
import pt.com.matreco.mmdb.api.model.tvSeries.TvDetails;
import pt.com.matreco.mmdb.api.model.tvSeries.TvSerieDetailsCredits;
import pt.com.matreco.mmdb.api.model.tvSeries.TvSerieSeasons;
import pt.com.matreco.mmdb.api.model.tvSeries.TvTrailer;
import pt.com.matreco.mmdb.api.model.tvSeries.TvTrailerResult;
import pt.com.matreco.mmdb.utils.MmdbConstants;
import pt.com.matreco.mmdb.utils.MmdbUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static pt.com.matreco.mmdb.utils.MmdbUtils.removeTrailingComma;

/**
 * Created by carlo on 20/12/2017.
 */

public class MainTvSeriesDetails extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {
    private Context mContext;

//    @BindView(R.id.tvseries_containerTitle)
//    View contentView;
    @BindView(R.id.tvseries_containerTitle)
    View contentViewTitle;
    @BindView(R.id.tvseries_imageView)
    ImageView imageView;
    @BindView(R.id.tvseries_overviewTextView)
    TextView overviewTextView;
    @BindView(R.id.tvseries_overviewHeader)
    TextView overviewHeaderTextView;
    @BindView(R.id.tvseries_genresTextView)
    TextView genresTextVieW;
    @BindView(R.id.tvseries_durationTextView)
    TextView durationTextView;
    @BindView(R.id.tvseries_languageTextView)
    TextView languageTextView;
    @BindView(R.id.tvseries_releaseDateTextView)
    TextView releaseDateTextView;
    @BindView(R.id.tvseries_titleTextView)
    TextView titleTextView;
    @BindView(R.id.loader)
    View loadingView;
    @BindView(R.id.tvseries_directorTextView)
    TextView directorTextView;
//    @BindView(R.id.tvseries_castTextView)
//    TextView castTextView;
    @BindView(R.id.tvseries_directorHeader)
    TextView directorHeader;
    @BindView(R.id.tvseries_castHeader)
    TextView castHeader;
    @BindView(R.id.tvseries_trailerHeader)
    TextView trailerHeader;
    @BindView(R.id.tvseries_btn_share)
    ImageButton btn_share;

    @BindView(R.id.horizontalScroll_linear)
    LinearLayout hSLinear;

    HorizontalLayoutForSeasonsAdapter horizontalLayoutForSeasonsAdapter;

    public String TRAILER_ID;
    public String TVSERIE_ID;
    public String POSTERPATH;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_tvseries_details);
        ButterKnife.bind(this);

        TVSERIE_ID = this.getIntent().getExtras().getString("tvSerieId");
        Log.d("ID", "----> " + TVSERIE_ID);

        showContent(false);
        getTvSerieDetails(TVSERIE_ID);
        getCast(TVSERIE_ID);
        getTvSerieTrailer(TVSERIE_ID);
    }

    public void getTvSerieDetails (final String tvSerieId){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.API_URL))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface apiI = retrofit.create(ApiInterface.class);
        Call<TvDetails> callTvSerieDetails = apiI.getTvSerieDetails(tvSerieId, getString(R.string.API_KEY),getString(R.string.API_LANGUAGE));

        callTvSerieDetails.enqueue(new Callback<TvDetails>() {
            @Override
            public void onResponse(Call<TvDetails> call, Response<TvDetails> response) {
                TvDetails rTvSeriesDetails = response.body();
                titleTextView.setText(rTvSeriesDetails.getName());
                genresTextVieW.setText(MmdbUtils.getTvSerieGenres(rTvSeriesDetails));
                durationTextView.setText(getAvarageDuration(rTvSeriesDetails));
                overviewTextView.setText(MmdbUtils.getOverview(rTvSeriesDetails.getOverview()));
                languageTextView.setText(MmdbUtils.getTvSeriesLanguages(rTvSeriesDetails));
                Picasso.with(mContext).load(getString(R.string.API_IMG_URL) + MmdbUtils.getFullImageUrl(rTvSeriesDetails.getPosterPath(),rTvSeriesDetails.getBackdropPath()))
                        .placeholder(R.mipmap.mmdb_ic_launcher_round)
                        .into(imageView);
                releaseDateTextView.setText(rTvSeriesDetails.getFirstAirDate());

                stopProgressBar();
                getTvSeriesSeasonPoster(rTvSeriesDetails.getNumberOfSeasons(),tvSerieId);

            }

            @Override
            public void onFailure(Call<TvDetails> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void getTvSeriesSeasonPoster(Integer numberOfSeasons,final String tvSerieId) {
        for (int serieIndex= 1 ; serieIndex<= numberOfSeasons; serieIndex++ ){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.API_URL))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface apiI = retrofit.create(ApiInterface.class);
        Call<TvSerieSeasons> callTvSerieSeasons = apiI.getTvSeriesSeasons(tvSerieId,serieIndex, getString(R.string.API_KEY),getString(R.string.API_LANGUAGE));
            Log.d("Season Number", "----> " + serieIndex);
        callTvSerieSeasons.enqueue(new Callback<TvSerieSeasons>() {
            @Override
            public void onResponse(Call<TvSerieSeasons> call, Response<TvSerieSeasons> response) {
                TvSerieSeasons rTvSeriesDetails = response.body();
                Log.d("Response", "----> " + response.raw());

                POSTERPATH = rTvSeriesDetails.getPosterPath();
                if (POSTERPATH != null) {
                    horizontalLayoutForSeasonsAdapter = findViewById(R.id.horizontalScroll_linear);

                    horizontalLayoutForSeasonsAdapter.add(getString(R.string.API_IMG_URL)+POSTERPATH, tvSerieId);
                }
            }

            @Override
            public void onFailure(Call<TvSerieSeasons> call, Throwable t) {
                t.printStackTrace();
            }
        });
            try {
                Thread.sleep(150);
            } catch(InterruptedException e) {

            }
        }
    }

    private String getAvarageDuration(TvDetails tvSerie){
        int runtime = 0;
        for (int i = 0; i < tvSerie.getEpisodeRunTime().size(); i++) {
            runtime += tvSerie.getEpisodeRunTime().get(i);
        }
        return runtime <= 0 ? "-" : getResources().getQuantityString(R.plurals.duration, runtime / tvSerie.getEpisodeRunTime().size(), runtime / tvSerie.getEpisodeRunTime().size());
    }

    public void getCast(String tvSerieId){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.API_URL))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface apiI = retrofit.create(ApiInterface.class);
        Call<TvSerieDetailsCredits> callCredits = apiI.getTvSerieDetailsCredits(tvSerieId, getString(R.string.API_KEY));

        callCredits.enqueue(new Callback<TvSerieDetailsCredits>() {
            @Override
            public void onResponse(Call<TvSerieDetailsCredits> call, Response<TvSerieDetailsCredits> response) {
                TvSerieDetailsCredits rTvSerieDetailsCredits = response.body();

                directorTextView.setText(getTvSerieCreator(rTvSerieDetailsCredits));
//                castTextView.setText(getMovieCast(rTvSerieDetailsCredits));
                getHorizontalCastScroll(rTvSerieDetailsCredits);

                rTvSerieDetailsCredits.getCrew();
//                stopProgressBar();
            }
            @Override
            public void onFailure(Call<TvSerieDetailsCredits> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void getHorizontalCastScroll(TvSerieDetailsCredits tvSerieCast){
        LinearLayout parent = findViewById(R.id.horizontalSerieCast);
        String actors ="";

        int j = tvSerieCast.getCast().size()>=10 ? 10 : tvSerieCast.getCast().size();

        for(int i = 0; i<j; i++) {
            pt.com.matreco.mmdb.api.model.tvSeries.Cast cast = tvSerieCast.getCast().get(i);
            actors = cast.getName()  + System.getProperty("line.separator") +  " (" + cast.getCharacter() + ")";

            LinearLayout layout2 = new LinearLayout(this);
            layout2.setLayoutParams(new LinearLayout.LayoutParams(250, LinearLayout.LayoutParams.WRAP_CONTENT));
            layout2.setOrientation(LinearLayout.VERTICAL);

            /*IMAGEM*/
            ImageView imageView = new ImageView(this);
            imageView.setLayoutParams(new LinearLayout.LayoutParams(210, 250));
            Bitmap bm = MmdbUtils.decodeSampledBitmapFromUri ("https://image.tmdb.org/t/p/w500"+cast.getProfilePath(),220,220);
            imageView.setImageBitmap(bm);
            imageView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorMainMenu));

            if(cast.getProfilePath()==null){
                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                Picasso.with(this).load(R.drawable.person_32_32)
                        .into(imageView);
            }else {
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                Picasso.with(this).load("https://image.tmdb.org/t/p/w500" + cast.getProfilePath())
                        .placeholder(R.drawable.person_32_32)
                        .into(imageView);
            }

            /*NOME*/
            Typeface font = Typeface.createFromAsset(getAssets(),"latobold.ttf");
            TextView textView = new TextView(this);
            textView.setText(actors);
            textView.setId(i);
            textView.setLayoutParams(new LinearLayout.LayoutParams(250, LinearLayout.LayoutParams.WRAP_CONTENT));
            textView.setTextSize(13);
            textView.setTypeface(font);
            textView.setTextColor(ContextCompat.getColor(this, R.color.colorText));
            textView.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);

            layout2.addView(imageView);
            layout2.addView(textView);
            parent.addView(layout2);
        }
    }

//    public Bitmap decodeSampledBitmapFromUri(String path, int reqWidth, int reqHeight) {
//        Bitmap bm = null;
//
//        // First decode with inJustDecodeBounds=true to check dimensions
//        final BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inJustDecodeBounds = true;
//        BitmapFactory.decodeFile(path, options);
//
//        // Calculate inSampleSize
//        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
//
//        // Decode bitmap with inSampleSize set
//        options.inJustDecodeBounds = false;
//        bm = BitmapFactory.decodeFile(path, options);
//
//        return bm;
//    }
//
//    public int calculateInSampleSize(
//
//            BitmapFactory.Options options, int reqWidth, int reqHeight) {
//        // Raw height and width of image
//        final int height = options.outHeight;
//        final int width = options.outWidth;
//        int inSampleSize = 1;
//
//        if (height > reqHeight || width > reqWidth) {
//            if (width > height) {
//                inSampleSize = Math.round((float) height / (float) reqHeight);
//            } else {
//                inSampleSize = Math.round((float) width / (float) reqWidth);
//            }
//        }
//
//        return inSampleSize;
//    }

    // TOP 5 ACTORS
    public String getMovieCast(TvSerieDetailsCredits tvSerieCast){
        String actors ="";
        int castSize = tvSerieCast.getCast().size() >=5 ? 5 : tvSerieCast.getCast().size();
        for (int i = 0; i < castSize; i++) {
            pt.com.matreco.mmdb.api.model.tvSeries.Cast cast = tvSerieCast.getCast().get(i);
            actors += cast.getName() + " (" + cast.getCharacter() + ")" + System.getProperty("line.separator");
        }
        actors = removeTrailingComma(actors);
        return  actors;
    }

    public String getTvSerieCreator(TvSerieDetailsCredits creator){
        String director ="";
            for (int i = 0; i < creator.getCrew().size(); i++) {
                Crew crew = creator.getCrew().get(i);
                if (crew.getJob().equals(MmdbConstants.TV_SERIE_CREATOR)) {
                    director += crew.getName() + ", ";
                }
            }
        director= removeTrailingComma(director);

        return director;
    }

    public void getTvSerieTrailer(String movieId){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.API_URL))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface apiI = retrofit.create(ApiInterface.class);
        Call<TvTrailer> callTrailer = apiI.getTvSerieTrailer(movieId, getString(R.string.API_KEY),null);

        callTrailer.enqueue(new Callback<TvTrailer>() {
            @Override
            public void onResponse(Call<TvTrailer> call, Response<TvTrailer> response) {
                TvTrailer rTvTrailer = response.body();
                List<TvTrailerResult> trailerList = rTvTrailer.getResults();

                if(trailerList.size()!= 0){
                    TvTrailerResult results = trailerList.get(0);
                    TRAILER_ID = results.getKey();
                    showTrailer(true);
                }else {
                    showTrailer(false);
                }

                stopProgressBar();

            }
            @Override
            public void onFailure(Call<TvTrailer> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    public void stopProgressBar() {
        loadingView.setVisibility(View.GONE);
        showContent(true);
    }

    // TODO atualizar o showContent com as novas views
    private void showContent(boolean show) {
        int visibility = show ? View.VISIBLE : View.INVISIBLE;

//        contentView.setVisibility(visibility);
        contentViewTitle.setVisibility(visibility);
        overviewHeaderTextView.setVisibility(visibility);
        overviewTextView.setVisibility(visibility);
        directorHeader.setVisibility(visibility);
        castHeader.setVisibility(visibility);
        directorTextView.setVisibility(visibility);
//        castTextView.setVisibility(visibility);
        trailerHeader.setVisibility(visibility);
        showTrailer(show);
    }

    /*YOUTUBE API*/
    public void showTrailer(boolean show){
        int visibility = show ? View.VISIBLE : View.INVISIBLE;
        YouTubePlayerView youTubePlayerView = findViewById(R.id.tvseries_youtube_player);
        youTubePlayerView.setVisibility(visibility);

        if(show){
            youTubePlayerView.initialize(getString(R.string.YOUTUBE_API_KEY), this);
        }

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult result) {
        Toast.makeText(this, "Failured to Initialize!", Toast.LENGTH_LONG).show();
    }
    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
        /** add listeners to YouTubePlayer instance **/
        player.setPlayerStateChangeListener(playerStateChangeListener);
        player.setPlaybackEventListener(playbackEventListener);
        /** Start buffering **/
        if (!wasRestored) {
            player.cueVideo(TRAILER_ID);
        }
    }

    private YouTubePlayer.PlaybackEventListener playbackEventListener = new YouTubePlayer.PlaybackEventListener() {
        @Override
        public void onBuffering(boolean arg0) {
        }
        @Override
        public void onPaused() {
        }
        @Override
        public void onPlaying() {
        }
        @Override
        public void onSeekTo(int arg0) {
        }
        @Override
        public void onStopped() {
        }
    };
    private YouTubePlayer.PlayerStateChangeListener playerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {
        @Override
        public void onAdStarted() {
        }
        @Override
        public void onError(YouTubePlayer.ErrorReason arg0) {
        }
        @Override
        public void onLoaded(String arg0) {
        }
        @Override
        public void onLoading() {
        }
        @Override
        public void onVideoEnded() {
        }
        @Override
        public void onVideoStarted() {
        }
    };
    /*YOUTUBE API ENDS*/

    /* SHARING TO SOCIAL MEDIA*/
    // TODO Adicionar texto de share aos resources
    @OnClick(R.id.tvseries_btn_share)
    public void shareToSocialMedia() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT,"TV Serie " + titleTextView.getText()  );
        startActivity(intent);
    }
}
