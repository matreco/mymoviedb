package pt.com.matreco.mmdb;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by carlo on 10/12/2017.
 */

public class MainMoviesMenuActivity extends AppCompatActivity {
    @BindView(R.id.search_movie_text)
    EditText search_movie_text;

    @BindView(R.id.btn_search_movie)
    ImageButton btn_search_movie;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_main_movies_menu_activity);
        ButterKnife.bind(this);

        addListeners();
        verifyInputLength();
    }


    private void addListeners() {
        search_movie_text.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    submiteSearch();
                    return true;
                }
                return false;
            }
        });
    }

    @OnClick(R.id.btn_search_movie)
    public void submiteSearch() {
            Intent intent = new Intent(this, MainMoviesActivity.class);
            intent.putExtra("type", "search");
            intent.putExtra("movieString", search_movie_text.getText().toString());
            Log.d("TAG ", search_movie_text.getText().toString());
            startActivity(intent);
            finish();
    }

    private void verifyInputLength() {
        btn_search_movie.setEnabled(search_movie_text.getText().toString().length() > 0);
    }

    @OnClick(R.id.btn_movies_popular)
    public void submitePopular() {
        Intent intent = new Intent(this, MainMoviesActivity.class);
        intent.putExtra("category", "popular");
        startActivity(intent);
//        finish();
    }

    @OnClick(R.id.btn_movies_topRated)
    public void submiteTopRated() {
        Intent intent = new Intent(this, MainMoviesActivity.class);
        intent.putExtra("category", "top_rated");
        startActivity(intent);
//        finish();
    }

    @OnClick(R.id.btn_movies_upComing)
    public void submiteUpComing() {
        Intent intent = new Intent(this, MainMoviesActivity.class);
        intent.putExtra("category", "upcoming");
        startActivity(intent);
//        finish();
    }

    @OnClick(R.id.btn_movies_nowPlaying)
    public void submiteNowPlaying() {
        Intent intent = new Intent(this, MainMoviesActivity.class);
        intent.putExtra("category", "now_playing");
        startActivity(intent);
//        finish();
    }
}

