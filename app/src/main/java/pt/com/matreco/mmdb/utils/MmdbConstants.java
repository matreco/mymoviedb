package pt.com.matreco.mmdb.utils;

/**
 * Created by carlo on 21/12/2017.
 */

public class MmdbConstants {
    public final static String TV_SERIE_CREATOR = "Executive Producer";

    public final static String MOVIE_DIRECTOR = "Director";
}
