package pt.com.matreco.mmdb.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import pt.com.matreco.mmdb.R;
import pt.com.matreco.mmdb.api.model.movies.Cast;
import pt.com.matreco.mmdb.api.model.movies.Crew;
import pt.com.matreco.mmdb.api.model.movies.MovieDetails;
import pt.com.matreco.mmdb.api.model.movies.MovieDetailsCredits;
import pt.com.matreco.mmdb.api.model.movies.SpokenLanguage;
import pt.com.matreco.mmdb.api.model.tvSeries.Genre;
import pt.com.matreco.mmdb.api.model.tvSeries.TvDetails;
import pt.com.matreco.mmdb.api.model.tvSeries.TvSerieDetailsCredits;

/**
 * Created by carlo on 20/12/2017.
 */

public final class MmdbUtils {
    private static final String TAG = MmdbUtils.class.getSimpleName();

    public static String getTvSerieGenres(TvDetails tvSerie) {
        String genres = "";
        for (int i = 0; i < tvSerie.getGenres().size(); i++) {
            Genre genre = tvSerie.getGenres().get(i);
            genres += genre.getName() + ", ";
        }
        genres = removeTrailingComma(genres);

        return genres.isEmpty() ? "-" : genres;
    }

    public static String removeTrailingComma(String text) {
        text = text.trim();
        if (text.endsWith(",")) {
            text = text.substring(0, text.length() - 1);
        }
        return text;
    }

    public static String getTvSeriesLanguages(TvDetails tvSerie) {
        String languages = "";
        for (int i = 0; i < tvSerie.getLanguages().size(); i++) {
            languages += tvSerie.getLanguages().get(i)+ ", ";
        }
        languages = removeTrailingComma(languages);
        return languages.isEmpty() ? "-" : languages;
    }


    public static String getOverview(String overview) {
        return TextUtils.isEmpty(overview) ? "-" : overview;
    }

    public static String getFullImageUrl(String posterPath, String backdropPath) {
        String imagePath;
        if (posterPath != null && !posterPath.isEmpty()) {
            imagePath = posterPath;
        } else {
            imagePath = backdropPath;
        }
        return imagePath;
    }

//    public static String getMovieDirector(MovieDetailsCredits credits){
//        String director ="";
//        for (int i = 0; i < credits.getCrew().size(); i++) {
//            Crew crew = credits.getCrew().get(i);
//            if(crew.getJob().equals("Director")){
//                director += crew.getName()+ ", ";
//            }
//        }
//        director= removeTrailingComma(director);
//
//        return director;
//    }
//
//    public static String getTvSerieDirector(TvSerieDetailsCredits credits){
//        String director ="";
//        for (int i = 0; i < credits.getCrew().size(); i++) {
//            Cast crew = credits.getCrew().get(i);
//            if(crew.getJob().equals("Director")){
//                director += crew.getName()+ ", ";
//            }
//        }
//        director= removeTrailingComma(director);
//
//        return director;
//    }

    // TOP 5 ACTORS
//    public String getMovieCast(MovieDetailsCredits movieCast){
//        String actors ="";
//        for (int i = 0; i < 5; i++) {
//            Cast cast = movieCast.getCast().get(i);
//            actors += cast.getName() + " (" + cast.getCharacter() + ")" + System.getProperty("line.separator");
//        }
//        actors = removeTrailingComma(actors);
//        return  actors;
//    }

    public static Bitmap decodeSampledBitmapFromUri(String path, int reqWidth, int reqHeight) {
        Bitmap bm = null;

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(path, options);

        return bm;
    }

    public static int calculateInSampleSize(

            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }

        return inSampleSize;
    }
}
