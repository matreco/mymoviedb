package pt.com.matreco.mmdb;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import pt.com.matreco.mmdb.adapters.MovieAdapter;
import pt.com.matreco.mmdb.adapters.TvSeriesAdapter;
import pt.com.matreco.mmdb.api.ApiInterface;
import pt.com.matreco.mmdb.api.model.movies.MovieResults;
import pt.com.matreco.mmdb.api.model.movies.Results;
import pt.com.matreco.mmdb.api.model.tvSeries.Result;
import pt.com.matreco.mmdb.api.model.tvSeries.TvResults;
import pt.com.matreco.mmdb.details.MainMovieDetails;
import pt.com.matreco.mmdb.details.MainTvSeriesDetails;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by carlo on 09/12/2017.
 */

public class MainTvSeriesActivity extends AppCompatActivity {
    @BindView(R.id.loader)
    View loadingView;
    @BindView(R.id.tvseries_list_item)
    GridView list_item;

    SwipeRefreshLayout mSwipeRefreshLayout;

    int mRPage= 1;

    List<Result> rlistOfTvSeries  = new ArrayList<>();
    TvSeriesAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_tvseries_listview);
        ButterKnife.bind(this);

        String CATEGORY = this.getIntent().getExtras().getString("category");
        mSwipeRefreshLayout = findViewById(R.id.activity_main_swipe_refresh_layout);
        adapter = new TvSeriesAdapter(this,rlistOfTvSeries);
        list_item.setAdapter(adapter);


        list_item.setOnScrollListener(new EndlessScrollListener(5) {
            @Override
            public void loadMore(int page, int totalItemsCount) {
                mRPage++;
                loadMoreTvSeries(mRPage);

            }
        });

        firstLoad(mRPage);
        adapter.notifyDataSetChanged();

        // DETAIL
        list_item.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Result r = (Result) rlistOfTvSeries.get(position);
                Intent myIntent = new Intent(MainTvSeriesActivity.this, MainTvSeriesDetails.class);
                myIntent.putExtra("tvSerieName", r.getOriginalName());
                myIntent.putExtra("tvSerieId", r.getId().toString());
                startActivity(myIntent);
            }

        });


        // TODO atualizar este codigo
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                adapter.clearData();
                adapter = new TvSeriesAdapter(getBaseContext(), rlistOfTvSeries);
                list_item.setAdapter(adapter);
                mRPage = 1;
                firstLoad(mRPage);
                adapter.notifyDataSetChanged();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    // TODO atualizar este codigo



    public void firstLoad(int page){
        String CATEGORY = this.getIntent().getExtras().getString("category");
        String TYPE = this.getIntent().getExtras().getString("type");
        String TVSERIESSTRING = this.getIntent().getExtras().getString("tvSerieString");

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.API_URL))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface apiI = retrofit.create(ApiInterface.class);

        // TODO implementar a pesquisa
        if(TYPE != null && !TYPE.isEmpty()){
            Call<TvResults> callTvSeriesResults = apiI.getSearchTvSeries(getString(R.string.API_KEY), getString(R.string.API_LANGUAGE),TVSERIESSTRING, mRPage);
            callTvSeriesResults.enqueue(new Callback<TvResults>() {
                @Override
                public void onResponse(Call<TvResults> call, Response<TvResults> response) {
                    final TvResults mResults = response.body();

                    if(mResults.getTotalResults()==0){
                        Intent myIntent = new Intent(MainTvSeriesActivity.this, MainTvSeriesMenuActivity.class);
                        startActivity(myIntent);
                        finish();

                    }
                    List<Result> listOfTvSeries = mResults.getResults();
                    stopProgressBar();
                    adapter.addAll(listOfTvSeries);
                }

                @Override
                public void onFailure(Call<TvResults> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        }else {
            Call<TvResults> callTvSeriesResults = apiI.getListOfTvSeries(CATEGORY, getString(R.string.API_KEY), getString(R.string.API_LANGUAGE), mRPage);
            callTvSeriesResults.enqueue(new Callback<TvResults>() {
                @Override
                public void onResponse(Call<TvResults> call, Response<TvResults> response) {
                    final TvResults mResults = response.body();
                    List<Result> listOfTvSeries = mResults.getResults();
                    stopProgressBar();
                    adapter.addAll(listOfTvSeries);
                }

                @Override
                public void onFailure(Call<TvResults> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    public void loadMoreTvSeries(int page){
        String CATEGORY = this.getIntent().getExtras().getString("category");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.API_URL))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface apiI = retrofit.create(ApiInterface.class);

        Call<TvResults> callTvSeriesResults = apiI.getListOfTvSeries(CATEGORY, getString(R.string.API_KEY), getString(R.string.API_LANGUAGE), mRPage);
        callTvSeriesResults.enqueue(new Callback<TvResults>() {
            @Override
            public void onResponse(Call<TvResults> call, Response<TvResults> response) {
                final TvResults mResults = response.body();
                List<Result> listOfTvSeries = mResults.getResults();
                stopProgressBar();
                adapter.addAll(listOfTvSeries);
            }

            @Override
            public void onFailure(Call<TvResults> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    public void stopProgressBar() {
        loadingView.setVisibility(View.GONE);
        showContent(true);
    }

    private void showContent(boolean show) {
        int visibility = show ? View.VISIBLE : View.INVISIBLE;
        list_item.setVisibility(visibility);
    }
}
