package pt.com.matreco.mmdb;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by carlo on 20/12/2017.
 */

public class MainTvSeriesMenuActivity extends AppCompatActivity{

    @BindView(R.id.search_tvSeries_text)
    EditText search_tvSeries_text;

    @BindView(R.id.btn_search_tvSeries)
    ImageButton btn_search_tvSeries;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_tvseries_menu_activity);
        ButterKnife.bind(this);

        addListeners();
        verifyInputLength();

    }

    private void addListeners() {


            search_tvSeries_text.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        submiteSearch();
                        return true;
                    }
                    return false;
                }
            });

//        search_tvSeries_text.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//                verifyInputLength();
//            }
//        });
    }

    @OnClick(R.id.btn_search_tvSeries)
    public void submiteSearch() {
        Intent intent = new Intent(this, MainTvSeriesActivity.class);
        intent.putExtra("type", "search");
        intent.putExtra("tvSerieString", search_tvSeries_text.getText().toString());
        Log.d("TAG ", search_tvSeries_text.getText().toString());
        startActivity(intent);
        finish();
    }

    private void verifyInputLength() {
        btn_search_tvSeries.setEnabled(search_tvSeries_text.getText().toString().length() > 0);
    }

    @OnClick(R.id.btn_tvSeries_popular)
    public void submitePopular() {
        Intent intent = new Intent(this, MainTvSeriesActivity.class);
        intent.putExtra("category", "popular");
        startActivity(intent);
    }

    @OnClick(R.id.btn_tvSeries_topRated)
    public void submiteTopRated() {
        Intent intent = new Intent(this, MainTvSeriesActivity.class);
        intent.putExtra("category", "top_rated");
        startActivity(intent);
    }

    @OnClick(R.id.btn_tvSeries_onTv)
    public void submiteOnAir() {
        Intent intent = new Intent(this, MainTvSeriesActivity.class);
        intent.putExtra("category", "on_the_air");
        startActivity(intent);
    }

    @OnClick(R.id.btn_tvSeries_airingToday)
    public void submiteAiringToday() {
        Intent intent = new Intent(this, MainTvSeriesActivity.class);
        intent.putExtra("category", "airing_today");
        startActivity(intent);
    }
}
