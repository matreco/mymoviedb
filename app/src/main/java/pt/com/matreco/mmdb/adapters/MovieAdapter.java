package pt.com.matreco.mmdb.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import pt.com.matreco.mmdb.MainTvSeriesSeasonDetailsActivity;
import pt.com.matreco.mmdb.R;
import pt.com.matreco.mmdb.api.model.movies.Results;

/**
 * Created by carlo on 10/12/2017.
 */

public class MovieAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;
    private List<Results> mDataSource;

    public MovieAdapter(Context context, List<Results> items) {
        mContext = context;
        mDataSource = items;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return mDataSource.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataSource.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.screen_movies_listitems, parent, false);
        }else {
            view = convertView;
        }

        Results r = (Results) getItem(position);

        TextView titleTextView = view.findViewById(R.id.movie_title);
        ImageView thumbnailImageView = view.findViewById(R.id.movie_thumbnail);
        TextView releaseDateView = view.findViewById(R.id.list_item_releaseDate);
//        TextView popularityView = view.findViewById(R.id.list_item_popularity);
        RatingBar ratingBar = view.findViewById(R.id.rating);

        titleTextView.setText(r.getOriginalTitle());
        releaseDateView.setText(r.getReleaseDate());
//        popularityView.setText(getPopularityString(r.getVoteAverage()));
        Picasso.with(mContext).load(getFullImageUrl(r)).placeholder(R.mipmap.mmdb_ic_launcher_round).into(thumbnailImageView);

        float rating= (float) ((r.getVoteAverage()*5) /10);
        ratingBar.setRating(rating);

        return view;
    }

    private String getPopularityString(double popularity) {
        java.text.DecimalFormat decimalFormat = new java.text.DecimalFormat("#.#");
        return decimalFormat.format(popularity);
    }

    private String getFullImageUrl(Results movie) {
        String imagePath;

        if (movie.getPosterPath() != null && !movie.getPosterPath().isEmpty()) {
            imagePath = movie.getPosterPath();
        } else {
            imagePath = movie.getBackdropPath();
        }
        return "https://image.tmdb.org/t/p/w500" + imagePath;
    }

    public void add(Results r) {
        mDataSource.add(r);
        notifyDataSetChanged();
    }

    public void addAll(List<Results> moveResults) {
        for (Results result : moveResults) {
            add(result);
        }
    }

    public void clearData() {
        mDataSource.clear();
    }
}
