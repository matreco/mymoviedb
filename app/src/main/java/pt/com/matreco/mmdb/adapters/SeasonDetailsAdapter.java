package pt.com.matreco.mmdb.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import pt.com.matreco.mmdb.R;
import pt.com.matreco.mmdb.api.model.movies.Results;
import pt.com.matreco.mmdb.api.model.tvSeries.Episode;

/**
 * Created by carlo on 10/12/2017.
 */

public class SeasonDetailsAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;
    private List<Episode> mDataSource;

    public SeasonDetailsAdapter(Context context, List<Episode> items) {
        mContext = context;
        mDataSource = items;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return mDataSource.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataSource.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.screen_tvseries_seasons_listitems, parent, false);
        }else {
            view = convertView;
        }

        Episode episode = (Episode) getItem(position);

        TextView episodeName = view.findViewById(R.id.tvseries_seasons_epsiode_name);
        TextView episodeOverview = view.findViewById(R.id.tvseries_seasons_episode_overview);
        TextView episodeAirdate = view.findViewById(R.id.tvseries_seasons_episode_airdate);
//        TextView episodeNumber = view.findViewById(R.id.tvseries_seasons_episode_overview);
        ImageView episodeThumbnail = view.findViewById(R.id.tvseries_seasons_episode_thumbnail);

        episodeName.setText(episode.getName());
        episodeOverview.setText(episode.getOverview());
        episodeAirdate.setText(episode.getAirDate());
//        releaseDateView.setText(r.getReleaseDate());
        Picasso.with(mContext).load(getFullImageUrl(episode)).placeholder(R.mipmap.mmdb_ic_launcher_round).into(episodeThumbnail);

        return view;
    }

    private String getFullImageUrl(Episode episode) {
        String imagePath;

        if (episode.getStillPath() != null && !episode.getStillPath().isEmpty()) {
            imagePath = episode.getStillPath();
        } else {
            return "";
        }
        return "https://image.tmdb.org/t/p/w500" + imagePath;
    }

    public void add(Episode r) {
        mDataSource.add(r);
        notifyDataSetChanged();
    }

    public void addAll(List<Episode> moveResults) {
        for (Episode episode : moveResults) {
            add(episode);
        }
    }
}
