package pt.com.matreco.mmdb.details;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pt.com.matreco.mmdb.R;
import pt.com.matreco.mmdb.adapters.RoundedCornersTransform;
import pt.com.matreco.mmdb.api.ApiInterface;
import pt.com.matreco.mmdb.api.model.movies.Cast;
import pt.com.matreco.mmdb.api.model.movies.Crew;
import pt.com.matreco.mmdb.api.model.movies.Genre;
import pt.com.matreco.mmdb.api.model.movies.MovieDetails;
import pt.com.matreco.mmdb.api.model.movies.MovieDetailsCredits;
import pt.com.matreco.mmdb.api.model.movies.MovieTrailer;
import pt.com.matreco.mmdb.api.model.movies.MovieTrailerResult;
import pt.com.matreco.mmdb.api.model.movies.SpokenLanguage;
import pt.com.matreco.mmdb.utils.MmdbConstants;
import pt.com.matreco.mmdb.utils.MmdbUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerView;

/**
 * Created by carlo on 10/12/2017.
 */

public class MainMovieDetails extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener{
    //AppCompatActivity  {
    private Context mContext;

    @BindView(R.id.containerDetails1)
    View contentView;
//    @BindView(R.id.containerTitle)
//    View contentViewTitle;
//    @BindView(R.id.imageView)
//    ImageView imageView;
    @BindView(R.id.overviewTextView)
    TextView overviewTextView;
    @BindView(R.id.overviewHeader)
    TextView overviewHeaderTextView;
    @BindView(R.id.genresTextView)
    TextView genresTextVieW;
    @BindView(R.id.durationTextView)
    TextView durationTextView;
    @BindView(R.id.languageTextView)
    TextView languageTextView;
//    @BindView(R.id.releaseDateTextView)
//    TextView releaseDateTextView;
    @BindView(R.id.titleTextView)
    TextView titleTextView;
    @BindView(R.id.loader)
    View loadingView;
    @BindView(R.id.directorTextView)
    TextView directorTextView;
//    @BindView(R.id.castTextView)
//    TextView castTextView;
    @BindView(R.id.directorHeader)
    TextView directorHeader;
    @BindView(R.id.castHeader)
    TextView castHeader;
    @BindView(R.id.trailerHeader)
    TextView trailerHeader;
    @BindView(R.id.btn_share_movie)
    ImageButton btn_share_movie;
    @BindView(R.id.headerImageView)
    ImageView headerImageView;

//@BindView(R.id.textView)
//    View errorView;

    //    public static final String API_KEY = "AIzaSyC1rOEzYXZ8WGJO_QFMzNCHlckmfNTixtI";
    public String TRAILER_ID;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_movie_details);
        ButterKnife.bind(this);

        String movieId = this.getIntent().getExtras().getString("movieId");
        Log.d("TAG","## ->   " + movieId);

        showContent(false);
        getMovieDetails(movieId);
        getCredits(movieId);
        getMovieTrailer(movieId);
//        getHorizontalCastScroll(rMovieDetailsCredits);
    }

    public void getHorizontalCastScroll(MovieDetailsCredits movieCast){
        LinearLayout parent = findViewById(R.id.horizontalCast);
        String actors ="";
        int j = movieCast.getCast().size()>=10 ? 10 : movieCast.getCast().size();

        for(int i = 0; i<j; i++) {
            Cast cast = movieCast.getCast().get(i);
            actors = cast.getName()  + System.getProperty("line.separator") +  " (" + cast.getCharacter() + ")";

            LinearLayout layout2 = new LinearLayout(this);
            layout2.setLayoutParams(new LinearLayout.LayoutParams(250, LinearLayout.LayoutParams.WRAP_CONTENT));
            layout2.setOrientation(LinearLayout.VERTICAL);

            /*IMAGEM*/
            ImageView imageView = new ImageView(this);
            imageView.setLayoutParams(new LinearLayout.LayoutParams(210, 250));
            Bitmap bm = decodeSampledBitmapFromUri ("https://image.tmdb.org/t/p/w500"+cast.getProfilePath(),220,220);
            imageView.setImageBitmap(bm);
            imageView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorMainMenu));

            if(cast.getProfilePath()==null){
                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                Picasso.with(this).load(R.drawable.person_32_32)
                        .into(imageView);
            }else {
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                Picasso.with(this).load("https://image.tmdb.org/t/p/w500" + cast.getProfilePath())
                        .placeholder(R.drawable.person_32_32)
                        .into(imageView);
            }

            /*NOME*/
            Typeface font = Typeface.createFromAsset(getAssets(),"latobold.ttf");
            TextView textView = new TextView(this);
            textView.setText(actors);
            textView.setId(i);
            textView.setLayoutParams(new LinearLayout.LayoutParams(250, LinearLayout.LayoutParams.WRAP_CONTENT));
            textView.setTextSize(13);
            textView.setTypeface(font);
            textView.setTextColor(ContextCompat.getColor(this, R.color.colorText));
            textView.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);

            layout2.addView(imageView);
            layout2.addView(textView);
            parent.addView(layout2);
        }
    }

    public Bitmap decodeSampledBitmapFromUri(String path, int reqWidth, int reqHeight) {
        Bitmap bm = null;

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        bm = BitmapFactory.decodeFile(path, options);

        return bm;
    }

    public int calculateInSampleSize(

            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }

        return inSampleSize;
    }

    public void getMovieDetails (String movieId){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.API_URL))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface apiI = retrofit.create(ApiInterface.class);
        Call<MovieDetails> callMovieDetails = apiI.getMovieDetails(movieId, getString(R.string.API_KEY),getString(R.string.API_LANGUAGE));

        callMovieDetails.enqueue(new Callback<MovieDetails>() {
            @Override
            public void onResponse(Call<MovieDetails> call, Response<MovieDetails> response) {
                MovieDetails rMovieDetails = response.body();
                Log.d("TAG","## ->   " + rMovieDetails.getTitle());
                titleTextView.setText(rMovieDetails.getTitle());
                genresTextVieW.setText(getGenres(rMovieDetails));
                durationTextView.setText(getDuration(rMovieDetails));
                overviewTextView.setText(getOverview(rMovieDetails.getOverview()));
                languageTextView.setText(getLanguages(rMovieDetails));
//                Picasso.with(mContext).load(getFullImageUrl(rMovieDetails)).placeholder(R.mipmap.mmdb_ic_launcher_round).into(imageView);
//                releaseDateTextView.setText(rMovieDetails.getReleaseDate());
                Picasso.with(mContext).load("https://image.tmdb.org/t/p/w500"+rMovieDetails.getBackdropPath()).placeholder(R.mipmap.mmdb_ic_launcher_round).into(headerImageView);

            }
            @Override
            public void onFailure(Call<MovieDetails> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    public void getCredits(String movieId){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.API_URL))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface apiI = retrofit.create(ApiInterface.class);
        Call<MovieDetailsCredits> callCredits = apiI.getMovieDetailsCredits(movieId, getString(R.string.API_KEY));

        callCredits.enqueue(new Callback<MovieDetailsCredits>() {
            @Override
            public void onResponse(Call<MovieDetailsCredits> call, Response<MovieDetailsCredits> response) {
                MovieDetailsCredits rMovieDetailsCredits = response.body();

                directorTextView.setText(getMovieDirector(rMovieDetailsCredits));
                //castTextView.setText(getMovieCast(rMovieDetailsCredits));
                getHorizontalCastScroll(rMovieDetailsCredits);

            }
            @Override
            public void onFailure(Call<MovieDetailsCredits> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    public void getMovieTrailer(String movieId){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.API_URL))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface apiI = retrofit.create(ApiInterface.class);
        Call<MovieTrailer> callTrailer = apiI.getMovieTrailer(movieId, getString(R.string.API_KEY),null);

        callTrailer.enqueue(new Callback<MovieTrailer>() {
            @Override
            public void onResponse(Call<MovieTrailer> call, Response<MovieTrailer> response) {
                MovieTrailer rMovieTrailer = response.body();
                List<MovieTrailerResult> trailerList = rMovieTrailer.getResults();

                if(trailerList.size()!= 0){
                    MovieTrailerResult results = trailerList.get(0);
                    TRAILER_ID = results.getKey();
                    showTrailer(true);
                }else {
                    showTrailer(false);
                }

                stopProgressBar();

            }
            @Override
            public void onFailure(Call<MovieTrailer> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    public String getMovieDirector(MovieDetailsCredits credits){
        String director ="";
        for (int i = 0; i < credits.getCrew().size(); i++) {
            Crew crew = credits.getCrew().get(i);
            if(crew.getJob().equals(MmdbConstants.MOVIE_DIRECTOR)){
                director += crew.getName()+ ", ";
            }
        }
        director= MmdbUtils.removeTrailingComma(director);

        return director;
    }

    public void stopProgressBar() {
        loadingView.setVisibility(View.GONE);
        showContent(true);
    }

    private void showContent(boolean show) {
        int visibility = show ? View.VISIBLE : View.INVISIBLE;

        contentView.setVisibility(visibility);
//        contentViewTitle.setVisibility(visibility);
        overviewHeaderTextView.setVisibility(visibility);
        overviewTextView.setVisibility(visibility);
        directorHeader.setVisibility(visibility);
        castHeader.setVisibility(visibility);
        directorTextView.setVisibility(visibility);
//        castTextView.setVisibility(visibility);
        trailerHeader.setVisibility(visibility);
        showTrailer(show);
    }

    private String getDuration(MovieDetails movie) {
        int runtime = movie.getRuntime() != null ? movie.getRuntime() : 0 ;
        return runtime <= 0 ? "-" : getResources().getQuantityString(R.plurals.duration, runtime, runtime);
    }

    private String getOverview(String overview) {
        return TextUtils.isEmpty(overview) ? "-" : overview;
    }

    private String getFullImageUrl(MovieDetails movie) {
        String imagePath;

        if (movie.getPosterPath() != null && !movie.getPosterPath().isEmpty()) {
            imagePath = movie.getPosterPath();
        } else {
            imagePath = movie.getBackdropPath();
        }
        return getString(R.string.API_IMG_URL) + imagePath;
    }

    private String getGenres(MovieDetails movie) {
        String genres = "";
        for (int i = 0; i < movie.getGenres().size(); i++) {
            Genre genre = movie.getGenres().get(i);
            genres += genre.getName()+ ", ";
        }
        genres = MmdbUtils.removeTrailingComma(genres);

        return genres.isEmpty() ? "-" : genres;
    }

    private String getLanguages(MovieDetails movie) {
        String languages = "";
        for (int i = 0; i < movie.getSpokenLanguages().size(); i++) {
            SpokenLanguage language = movie.getSpokenLanguages().get(i);
            languages += language.getName()+ ", ";
        }

        languages = MmdbUtils.removeTrailingComma(languages);

        return languages.isEmpty() ? "-" : languages;
    }

    /* SHARING TO SOCIAL MEDIA*/
    // TODO Adicionar texto de share aos resources
    @OnClick(R.id.btn_share_movie)
    public void shareToSocialMedia() {
//        sharingToSocialMedia("com.facebook.katana");
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT,"Filme " + titleTextView.getText()  );
        startActivity(intent);
    }

//    public void sharingToSocialMedia(String application) {
//        Intent intent = new Intent(Intent.ACTION_SEND);
//        intent.setType("text/plain");
//        intent.putExtra(Intent.EXTRA_TEXT,"the force is with http://www.google.com or maybe not" );
//        boolean installed = checkAppInstall(application);
//        if (installed) {
//            intent.setPackage(application);
//            startActivity(intent);
//        } else {
//            Toast.makeText(getApplicationContext(),
//                    "Installed application first", Toast.LENGTH_LONG).show();
//        }
//    }


//    private boolean checkAppInstall(String uri) {
//        PackageManager pm = getPackageManager();
//        try {
//            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
//            return true;
//        } catch (PackageManager.NameNotFoundException e) {
//        }
//
//        return false;
//    }

    /*SHARING TO SOCIAL MEDIA ENDS*/

    /*YOUTUBE API*/
    public void showTrailer(boolean show){
        int visibility = show ? View.VISIBLE : View.INVISIBLE;
        YouTubePlayerView youTubePlayerView = findViewById(R.id.youtube_player);
        youTubePlayerView.setVisibility(visibility);

        if(show){
            youTubePlayerView.initialize(getString(R.string.YOUTUBE_API_KEY), this);
        }

    }

    @Override
    public void onInitializationFailure(Provider provider, YouTubeInitializationResult result) {
        Toast.makeText(this, "Failured to Initialize!", Toast.LENGTH_LONG).show();
    }
    @Override
    public void onInitializationSuccess(Provider provider, YouTubePlayer player, boolean wasRestored) {
        /** add listeners to YouTubePlayer instance **/
        player.setPlayerStateChangeListener(playerStateChangeListener);
        player.setPlaybackEventListener(playbackEventListener);
        /** Start buffering **/
        if (!wasRestored) {
            player.cueVideo(TRAILER_ID);
        }
    }


    private YouTubePlayer.PlaybackEventListener playbackEventListener = new YouTubePlayer.PlaybackEventListener() {
        @Override
        public void onBuffering(boolean arg0) {
        }
        @Override
        public void onPaused() {
        }
        @Override
        public void onPlaying() {
        }
        @Override
        public void onSeekTo(int arg0) {
        }
        @Override
        public void onStopped() {
        }
    };
    private YouTubePlayer.PlayerStateChangeListener playerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {
        @Override
        public void onAdStarted() {
        }
        @Override
        public void onError(YouTubePlayer.ErrorReason arg0) {
        }
        @Override
        public void onLoaded(String arg0) {
        }
        @Override
        public void onLoading() {
        }
        @Override
        public void onVideoEnded() {
        }
        @Override
        public void onVideoStarted() {
        }
    };
    /*YOUTUBE API ENDS*/
}